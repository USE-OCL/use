-----------------------------------------------------------------------------
--open C:\Users\Xuan-Loi Vu\Desktop\Parser\sc2eha\sc2eha.use
!create rc: RuleCollection 
------------------------------------------------------- INIT - OFF
-- sc2eha_initTop_coEvol.cmd
--matchSL:Tuple(_s_name:String)
!let matchSL = Tuple{_s_name:'Off'}
read C:\Users\Xuan-Loi Vu\Desktop\Parser\sc2eha\sc2eha_initTop_coEvol.cmd
------------------------------------------------------- ON
-- sc2eha_concurrStateTop_coEvol.cmd
--matchSL:Tuple(sc:Statechart,_s_name:String,_s1_name:String,_s2_name:String)
--matchTL:Tuple(eha:EHA,aut:AutH)
--matchCL:Tuple(s2e:SC2EHA)
!let sc = Statechart.allInstances->any(true)
!let eha = EHA.allInstances->any(true)
!let topAut = AutH.allInstances->any(true)
!let s2e = SC2EHA.allInstances->any(true)
!let matchSL = Tuple{sc:sc,_s_name:'On',_s1_name:'Lamp',_s2_name:'Camera'}
!let matchTL = Tuple{eha:eha,aut:topAut}
!let matchCL = Tuple{s2e:s2e}
read C:\Users\Xuan-Loi Vu\Desktop\Parser\sc2eha\sc2eha_concurrStateTop_coEvol.cmd
------------------------------------------------------- Lamp
-- sc2eha_initNest_coEvol.cmd
--matchSL:Tuple(sc:Statechart,cps:CompState,_s_name:String)
--matchTL:Tuple(eha:EHA,aut:AutH)
--matchCL:Tuple(s2e:SC2EHA,s2a:St2Aut)
!let lampState = CompState.allInstances->select(name='Lamp')->any(true)
!let lampCorr = lampState.cp_L_CompState_St2Aut->any(true)
!let lampAut = lampCorr.eha
!let matchSL = Tuple{sc:sc,cps:lampState,_s_name:'Green'}
!let matchTL = Tuple{eha:eha,aut:lampAut}
!let matchCL = Tuple{s2e:s2e,s2a:lampCorr}
read C:\Users\Xuan-Loi Vu\Desktop\Parser\sc2eha\sc2eha_initNest_coEvol.cmd
------------------------------------------------------- Camera
-- sc2eha_initNest_coEvol.cmd
--matchSL:Tuple(sc:Statechart,cps:CompState,_s_name:String)
--matchTL:Tuple(eha:EHA,aut:AutH)
--matchCL:Tuple(s2e:SC2EHA,s2a:St2Aut)
!let cameraState = CompState.allInstances->select(name='Camera')->any(true)
!let cameraCorr = cameraState.cp_L_CompState_St2Aut->any(true)
!let cameraAut = cameraCorr.eha
!let matchSL = Tuple{sc:sc,cps:cameraState,_s_name:'CameraOff'}
!let matchTL = Tuple{eha:eha,aut:cameraAut}
!let matchCL = Tuple{s2e:s2e,s2a:cameraCorr}
read C:\Users\Xuan-Loi Vu\Desktop\Parser\sc2eha\sc2eha_initNest_coEvol.cmd
------------------------------------------------------- Off_2_On
-- sc2eha_transitToConcurr_coEvol.cmd
--matchSL:Tuple(sc:Statechart,s1:State,s2:CompState,s3:State,s4:State,e:Event)
--matchTL:Tuple(eha:EHA,sH1:StateH,sH2:StateH,sH3:InitH,sH4:InitH,aut:AutH,eH:EventH)
--matchCL:Tuple(s2e:SC2EHA,s2sH1:S2SH,s2sH2:S2SH,s2sH3:S2SH,s2sH4:S2SH)
!let offState = State.allInstances->select(name='Off')->any(true)
!let onState = CompState.allInstances->select(name='On')->any(true)
!let greenState = State.allInstances->select(name='Green')->any(true)
!let cameraOff = State.allInstances->select(name='CameraOff')->any(true)
!let aut = AutH.allInstances->select(name='')->any(true)
!let s2sH1 = offState.cp_L_State_S2SH->any(true)
!let sH1 = s2sH1.eha
!let s2sH2 = onState.cp_L_State_S2SH->any(true)
!let sH2 = s2sH2.eha
!let s2sH3 = greenState.cp_L_State_S2SH->any(true)
!let sH3 = s2sH3.eha->oclAsType(InitH)
!let s2sH4 = cameraOff.cp_L_State_S2SH->any(true)
!let sH4 = s2sH4.eha->oclAsType(InitH)
!create switch:Event
!set switch.name:='Switch'
!create switchH:EventH
!set switchH.name:='Switch'
!let matchSL = Tuple{sc:sc,s1:offState,s2:onState,s3:greenState,s4:cameraOff,e:switch}
!let matchTL = Tuple{eha:eha,sH1:sH1,sH2:sH2,sH3:sH3,sH4:sH4,aut:aut,eH:switchH}
!let matchCL = Tuple{s2e:s2e,s2sH1:s2sH1,s2sH2:s2sH2,s2sH3:s2sH3,s2sH4:s2sH4}
read C:\Users\Xuan-Loi Vu\Desktop\Parser\sc2eha\sc2eha_transitToConcurr_coEvol.cmd
------------------------------------------------------- Yellow
-- sc2eha_simpStateNest_coEvol.cmd
--matchSL:Tuple(sc:Statechart,cps:CompState,_s_name:String)
--matchTL:Tuple(eha:EHA,aut:AutH)
--matchCL:Tuple(s2e:SC2EHA,s2a:St2Aut)
!let matchSL = Tuple{sc:sc,cps:lampState,_s_name:'Yellow'}
!let matchTL = Tuple{eha:eha,aut:lampAut}
!let matchCL = Tuple{s2e:s2e,s2a:lampCorr}
read C:\Users\Xuan-Loi Vu\Desktop\Parser\sc2eha\sc2eha_simpStateNest_coEvol.cmd
------------------------------------------------------- TimeGreen
-- sc2eha_transitToSimp_coEvol.cmd
--matchSL:Tuple(sc:Statechart,s1:State,s2:State,e:Event)
--matchTL:Tuple(eha:EHA,sH1:StateH,sH2:StateH,aut:AutH,eH:EventH)
--matchCL:Tuple(s2e:SC2EHA,s2sH1:S2SH,s2sH2:S2SH)
!let yellowState = State.allInstances->select(name='Yellow')->any(true)
!create timeGreen:Event
!set timeGreen.name:='TimeGreen'
!create timeGreenH:EventH
!set timeGreenH.name:='TimeGreen'
!let s2sH1 = greenState.cp_L_State_S2SH->any(true)
!let sH1 = s2sH1.eha
!let s2sH2 = yellowState.cp_L_State_S2SH->any(true)
!let sH2 = s2sH2.eha
!let matchSL = Tuple{sc:sc,s1:greenState,s2:yellowState,e:timeGreen}
!let matchTL = Tuple{eha:eha,sH1:sH1,sH2:sH2,aut:lampAut,eH:timeGreenH}
!let matchCL = Tuple{s2e:s2e,s2sH1:s2sH1,s2sH2:s2sH2}
read C:\Users\Xuan-Loi Vu\Desktop\Parser\sc2eha\sc2eha_transitToSimp_coEvol.cmd
------------------------------------------------------- Red
-- sc2eha_compStateNest_coEvol.cmd
--matchSL:Tuple(sc:Statechart,cps:CompState,_s_name:String)
--matchTL:Tuple(eha:EHA,aut:AutH,_aut1_name:String)
--matchCL:Tuple(s2e:SC2EHA,s2a:St2Aut)
!let matchSL = Tuple{sc:sc,cps:lampState,_s_name:'Red'}
!let matchTL = Tuple{eha:eha,aut:lampAut,_aut1_name:'Counter'}
!let matchCL = Tuple{s2e:s2e,s2a:lampCorr}
read C:\Users\Xuan-Loi Vu\Desktop\Parser\sc2eha\sc2eha_compStateNest_coEvol.cmd
------------------------------------------------------- Counter
-- sc2eha_initNest_coEvol.cmd
--matchSL:Tuple(sc:Statechart,cps:CompState,_s_name:String)
--matchTL:Tuple(eha:EHA,aut:AutH)
--matchCL:Tuple(s2e:SC2EHA,s2a:St2Aut)
!let redState = CompState.allInstances->select(name='Red')->any(true)
!let counterCorr = redState.cp_L_CompState_St2Aut->any(true)
!let counterAut = counterCorr.eha
!let matchSL = Tuple{sc:sc,cps:redState,_s_name:'Count0'}
!let matchTL = Tuple{eha:eha,aut:counterAut}
!let matchCL = Tuple{s2e:s2e,s2a:counterCorr}
read C:\Users\Xuan-Loi Vu\Desktop\Parser\sc2eha\sc2eha_initNest_coEvol.cmd
------------------------------------------------------- TimeYellow
-- sc2eha_transitToComp_coEvol.cmd
--matchSL:Tuple(sc:Statechart,s1:State,s2:CompState,s3:State,e:Event)
--matchTL:Tuple(eha:EHA,sH1:StateH,sH2:StateH,sH3:InitH,aut:AutH,eH:EventH)
--matchCL:Tuple(s2e:SC2EHA,s2sH1:S2SH,s2sH2:S2SH,s2sH3:S2SH)
!let count0 = State.allInstances->select(name='Count0')->any(true)
!let aut = AutH.allInstances->select(name='Lamp')->any(true)
!let s2sH1 = yellowState.cp_L_State_S2SH->any(true)
!let sH1 = s2sH1.eha
!let s2sH2 = redState.cp_L_State_S2SH->any(true)
!let sH2 = s2sH2.eha
!let s2sH3 = count0.cp_L_State_S2SH->any(true)
!let sH3 = s2sH3.eha->oclAsType(InitH)
!create timeYellow:Event
!set timeYellow.name:='TimeYellow'
!create timeYellowH:EventH
!set timeYellowH.name:='TimeYellow'
!let matchSL = Tuple{sc:sc,s1:yellowState,s2:redState,s3:count0,e:timeYellow}
!let matchTL = Tuple{eha:eha,sH1:sH1,sH2:sH2,sH3:sH3,aut:aut,eH:timeYellowH}
!let matchCL = Tuple{s2e:s2e,s2sH1:s2sH1,s2sH2:s2sH2,s2sH3:s2sH3}
read C:\Users\Xuan-Loi Vu\Desktop\Parser\sc2eha\sc2eha_transitToComp_coEvol.cmd
------------------------------------------------------- Count2
-- sc2eha_simpStateNest_coEvol.cmd
--matchSL:Tuple(sc:Statechart,cps:CompState,_s_name:String)
--matchTL:Tuple(eha:EHA,aut:AutH)
--matchCL:Tuple(s2e:SC2EHA,s2a:St2Aut)
!let matchSL = Tuple{sc:sc,cps:redState,_s_name:'Count2'}
!let matchTL = Tuple{eha:eha,aut:counterAut}
!let matchCL = Tuple{s2e:s2e,s2a:counterCorr}
read C:\Users\Xuan-Loi Vu\Desktop\Parser\sc2eha\sc2eha_simpStateNest_coEvol.cmd
------------------------------------------------------- RedYellow
-- sc2eha_simpStateNest_coEvol.cmd
--matchSL:Tuple(sc:Statechart,cps:CompState,_s_name:String)
--matchTL:Tuple(eha:EHA,aut:AutH)
--matchCL:Tuple(s2e:SC2EHA,s2a:St2Aut)
!let matchSL = Tuple{sc:sc,cps:lampState,_s_name:'RedYellow'}
!let matchTL = Tuple{eha:eha,aut:lampAut}
!let matchCL = Tuple{s2e:s2e,s2a:lampCorr}
read C:\Users\Xuan-Loi Vu\Desktop\Parser\sc2eha\sc2eha_simpStateNest_coEvol.cmd
------------------------------------------------------- CarStop
-- sc2eha_transitUpSimp_coEvol.cmd
--matchSL:Tuple(sc:Statechart,s1:State,s2:State,s3:CompState,e:Event)
--matchTL:Tuple(eha:EHA,sH1:StateH,sH2:StateH,sH3:StateH,aut:AutH,eH:EventH)
--matchCL:Tuple(s2e:SC2EHA,s2sH1:S2SH,s2sH2:S2SH,s2sH3:S2SH)
!let counter2 = State.allInstances->select(name='Count2')->any(true)
!let redYellow = State.allInstances->select(name='RedYellow')->any(true)
!let s2sH1 = counter2.cp_L_State_S2SH->any(true)
!let sH1 = s2sH1.eha
!let s2sH2 = redYellow.cp_L_State_S2SH->any(true)
!let sH2 = s2sH2.eha
!let s2sH3 = redState.cp_L_State_S2SH->any(true)
!let sH3 = s2sH3.eha
!create carStop:Event
!set carStop.name:='CarStop'
!create carStopH:EventH
!set carStopH.name:='CarStop'
!let matchSL = Tuple{sc:sc,s1:counter2,s2:redYellow,s3:redState,e:carStop}
!let matchTL = Tuple{eha:eha,sH1:sH1,sH2:sH2,sH3:sH3,aut:lampAut,eH:carStopH}
!let matchCL = Tuple{s2e:s2e,s2sH1:s2sH1,s2sH2:s2sH2,s2sH3:s2sH3}
read C:\Users\Xuan-Loi Vu\Desktop\Parser\sc2eha\sc2eha_transitUpSimp_coEvol.cmd
------------------------------------------------------- Red_Init
--hanhdd_--read C:\Users\Xuan-Loi Vu\Desktop\Parser\sc2eha\sc2eha_initNest_coEvol.cmd
------------------------------------------------------- Count0
--hanhdd_--read C:\Users\Xuan-Loi Vu\Desktop\Parser\sc2eha\sc2eha_simpNest_coEvol.cmd
------------------------------------------------------- Count1
--hanhdd_--read C:\Users\Xuan-Loi Vu\Desktop\Parser\sc2eha\sc2eha_simpNest_coEvol.cmd
------------------------------------------------------- ON_R_Init
--hanhdd_--read C:\Users\Xuan-Loi Vu\Desktop\Parser\sc2eha\sc2eha_initNest_coEvol.cmd
------------------------------------------------------- CameraOn
--hanhdd_--read C:\Users\Xuan-Loi Vu\Desktop\Parser\sc2eha\sc2eha_simpNest_coEvol.cmd
------------------------------------------------------- CameraOff
--hanhdd_--read C:\Users\Xuan-Loi Vu\Desktop\Parser\sc2eha\sc2eha_simpNest_coEvol.cmd
------------------------------------------------------- Init2Green
--hanhdd_--read C:\Users\Xuan-Loi Vu\Desktop\Parser\sc2eha\sc2eha_transitToSimp_coEvol.cmd
------------------------------------------------------- TimeGreen
--hanhdd_--read C:\Users\Xuan-Loi Vu\Desktop\Parser\sc2eha\
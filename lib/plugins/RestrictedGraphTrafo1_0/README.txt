1. Requirements
===============
-	USE version 3.0.0 is required for this plugin

2. Installation
===============
Please, copy the RetrictedGraphTrafo.jar into the lib/plugins directory of USE.

3. Usage
===============
-	Open USE specification.
-	Choose the Retricted Graph Trafo Parser icon on the USE toolbars,
	and select the input file (the Restricted Graph Trafo Language (RTL)
	specificiation for model transformation). After that, the RetrictedGraphTrafo
	plugin will compile the input file and parse the input to the USE model,
	Triple rules and USE command files.
-	For each triple rule, the plugin parses the rule into two operations for model
	transformation and model co-evolution (forwTrafo and coEvol), and organizes
	all operations as methods of a common class RuleCollection. Also,
	two USE command files are generated to present the coEvol and forwTrafo operations.
	We can use these USE command files to perform a model transformation scenario.
	The SC2EHA transformation gives you an example to use them.
-	When an input is parsed successfully, the USE model and all triple rules will
	be automatically loaded in the USE tool. A triple rule tree is used to manage
	all rules. A triple rule can be visualized in the USE by selecting an item of
	the tree.
-	We also give an additional feature to the plugin. We can visualize a triple
	rules description with the plugin. Choose the Triple rule icon on the USE toolbars,
	and select the triple rules description file. After that, we can visualize each
	rule in the USE tool.

4. Example
We give a model transformation example with the RetrictedGraphTrafo plugin: the SC2EHA
transformation. It is the transformation from the UML statechart to the Extended
Hirarchical Automaton. We study the transformation in the context where model is
used to specify a super traffic light system. Further more information can be found
in the RestrictedGraphTrafo - Guide.pdf. The file is attached to the
RetrictedGraphTrafo ZIP archive.
